module.exports = grunt => {
    // load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-react');

    grunt.initConfig({

        connect: {
            serve: {
                options: {
                    port: 3000,
                    base: '',
                    open: true
                }
            }
        },

        watch: {
            scripts: {
                files: '**/*.jsx',
                tasks: ['react'],
                options: {
                    livereload: true
                }
            }
        },

        babel: {
            options: {
                sourceMap: true,
                presets: ['es2015', 'react']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'jsx',
                    src: ['**/*.jsx'],
                    dest: 'js',
                    ext: '.js'
                }]
            }
        }
        
    });

    grunt.registerTask('serve', [
        'connect:serve',
        'babel',
        'watch'
    ]);
};
