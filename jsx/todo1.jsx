
var TodoList = React.createClass({

	getInitialState: function(){
		return {actions: ['aaaa', 'bbb']};
	},

	update: function(newText, idx){
		var arr = this.state.actions;
		arr[idx] = newText;
		this.setState({actions: arr});
	},

	remove: function(newText, idx){
		var arr = this.state.actions;
		arr.splice(idx, 1);
		this.setState({actions: arr});
	},

	add: function(){
		var arr = this.state.actions;
		arr.push('default text');
		this.setState({actions: arr});
	},

	search: function(){
		this.setState({search: this.refs.search.value});
	},

	render: function(){
		var that = this;

		var filteredActions = !that.state.search ? this.state.actions : this.state.actions.filter(function(text){
			return text.indexOf(that.state.search) !== -1;
		});

		return (
			<div className="todo-list">
				<h1>TodoList</h1>
				search: <input ref="search" onChange={this.search}/>
				<button onClick={this.add}>+</button>
				{
					filteredActions.map(function(text, idx){
						return <Action text={text} key={idx} index={idx} update={that.update} remove={that.remove} />
					})
				}
			</div>
		);
	}

});	


var Action = React.createClass({

	getInitialState: function(){
		return ({editMode: false});
	},

	edit: function(){
		this.setState({editMode: true});
	},

	save: function(){
		this.setState({editMode: false});
		this.props.update(this.refs.newText.value, this.props.index);
	},

	remove: function(){
		this.props.remove(this.props.index);
	},

	render: function(){

		var readMode = 
			<div className="action">
				<span>{this.props.text}</span>
				<button onClick={this.edit}>edit</button>
				<button onClick={this.remove}>-</button>
			</div>;

		var editMode = 
			<div className="action">
				<input defaultValue={this.props.text} ref="newText" />
				<button onClick={this.save}>save</button>
			</div>;
		
		
		if (this.state.editMode) 
			return editMode;
		return readMode;
		
	}

});	

ReactDOM.render(<TodoList />, document.getElementById('content'));