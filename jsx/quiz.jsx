var Quiz = React.createClass({

	getInitialState: function(){
		return ({
			questions: [
				{
					text: '1+1 ?',
					answers: [1,2,3,4],
					correct: 1,
					selected: -1
				},
				{
					text: '2+2 ?',
					answers: [12,332,43,4],
					correct: 3,
					selected: -1
				},
				{
					text: '3+4 ?',
					answers: [7,662,443,554],
					correct: 0,
					selected: -1
				}
			],
			index: 0
		});
	},

	next: function(){
		var idx = ++this.state.index;
		this.setState({questions: this.state.questions, index: idx});
	},

	prev: function(){
		var idx = --this.state.index;
		this.setState({questions: this.state.questions, index: idx});
	},

	selectAnswer: function(idx){
		var arr = this.state.questions;
		arr[this.state.index].selected = idx;
		this.setState({questions: arr, index: this.state.index});
	},

	done: function(){
		var res = 0;
		this.state.questions.forEach(function(q){
			if (q.selected == q.correct) res++;
		});
		alert('correct: ' + res);
	},
	
	render: function(){
		var that = this;
		return (

			<div className="quiz">
				<h1>Quiz</h1>
				<h3>{this.state.questions[this.state.index].text}</h3>
				{
					this.state.questions[this.state.index].answers.map(function(answer, idx){
						return (<div key={idx}>
									<input 
										type="radio" 
										name="answer" 
										checked={that.state.questions[that.state.index].selected==idx} 
										onClick={that.selectAnswer.bind(null, idx)}/> 
									{answer}
								</div>);
					})
				}
				<button disabled={this.state.index==0} onClick={this.prev}>prev</button>
				<button disabled={this.state.index==(this.state.questions.length-1)} onClick={this.next}>next</button>
				{this.state.index==(this.state.questions.length-1) ? <button onClick={this.done}>done</button> : null}
			</div>

		);
	}
});
 
ReactDOM.render(<Quiz />, document.getElementById('content'));