// import React from 'react'

// class TodoList extends React.Component {

// 	constructor() {
// 		super();
// 		this.state = {
// 			actions: ['aaaa1', 'bbb']
// 		}
// 	}

// 	update(newText, idx){
// 		var arr = this.state.actions;
// 		arr[idx] = newText;
// 		this.setState({actions: arr});
// 	}

// 	remove(newText, idx){
// 		var arr = this.state.actions;
// 		arr.splice(idx, 1);
// 		this.setState({actions: arr});
// 	}

// 	add(){
// 		var arr = this.state.actions;
// 		arr.push('default text');
// 		this.setState({actions: arr});
// 	}

// 	search(){
// 		this.setState({search: this.refs.search.value});
// 	}

// 	render(){
// 		var that = this;

// 		var filteredActions = !that.state.search ? this.state.actions : this.state.actions.filter((text)=>{
// 			return text.indexOf(that.state.search) !== -1;
// 		});

// 		return (
// 			<div className="todo-list">
// 				<h1>TodoList</h1>
// 				search: <input ref="search" onChange={this.search}/>
// 				<button onClick={this.add}>+</button>
// 				{
// 					filteredActions.map(function(text, idx){
// 						return <div key={idx}>{text}</div>// <Action text={text} key={idx} index={idx} update={that.update} remove={that.remove} />
// 					})
// 				}
// 			</div>
// 		);
// 	}

// };

// export default TodoList;