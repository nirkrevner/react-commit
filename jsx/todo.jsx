var Action = React.createClass({

	getInitialState: function(){
		return ({edit: false});
	},

	edit: function(){
		this.setState({edit: true});
	},

	save: function(){
		this.props.edit(this.refs.editedText.value, this.props.index);
		this.setState({edit: false});
	},
	
	render: function(){
		var html;
		if (!this.state.edit){
			html = 	<h3>
						{this.props.children} 
						<button onClick={this.props.remove.bind(null, this.props.index)}>-</button>
						<button onClick={this.edit}>edit</button>					
					</h3>
		} else {
			html = 	<h3>
						<input defaultValue={this.props.children} ref="editedText"/>
						<button onClick={this.save}>save</button>
					</h3>
		}

		return (

			<div className="action">
				{html}				
			</div>

			);
	}
});

var TodoList = React.createClass({

	getInitialState: function(){
		return ({actions: [{text: 'hello'}]});
	},

	add: function(){
		var arr = this.state.actions;
		arr.push({text: 'default text'});
		this.setState({actions: arr});
	},

	remove: function(idx){
		var arr = this.state.actions;
		arr.splice(idx, 1);
		this.setState({actions: arr});
	},

	edit: function(text, idx){
		var arr = this.state.actions;
		arr[idx].text = text;
		this.setState({actions: arr});
	},
	
	render: function(){
		var that = this;
		return (

			<div className="todo-list">
				<h1>TODO List</h1>
				<button onClick={this.add}>+</button>
				{
					this.state.actions.map(function(action, idx){
						return (<Action key={idx} index={idx} remove={that.remove} edit={that.edit}>{action.text}</Action>);
					})
				}
			</div>

			);
	}
});
 
ReactDOM.render(<TodoList />, document.getElementById('content'));